#include <SDL2/SDL.h>

#include <fmt/format.h>

#include <chrono>
#include <fstream>
#include <future>
#include <iostream>
#include <memory>
#include <thread>
#include <vector>
#include <numbers>

#include "ctpl_stl.h"

using namespace std::chrono_literals;

const int WINDOW_WIDTH = 800;
const int WINDOW_HEIGHT = 600;
const int BALL_RAD = 10;
const int NUM_BALLS = 30;

const std::string FONT = "/usr/share/fonts/TTF/Hack Regular Nerd Font Complete.ttf";

class Ball {
 public:
  int x, y, vx, vy;

  Ball(int x_, int y_, int vx_, int vy_) : x(x_), y(y_), vx(vx_), vy(vy_) {}

  void update(double dt) {
    x += vx * dt;
    y += vy * dt;

    if (x < BALL_RAD) {
      vx = -vx;
      x = BALL_RAD;
    } else if (x + BALL_RAD > WINDOW_WIDTH) {
      vx = -vx;
      x = WINDOW_WIDTH - BALL_RAD;
    }
    if (y < BALL_RAD) {
      vy = -vy;
      y = BALL_RAD;
    } else if (y + BALL_RAD > WINDOW_HEIGHT) {
      vy = -vy;
      y = WINDOW_HEIGHT - BALL_RAD;
    }
  }
};

int idx(int x, int y) { return x + y * WINDOW_WIDTH; }

double get_rand() {
  // std::cout << "getting some random datas" << "\n";
  static std::ifstream is("/dev/urandom");
  auto res = (is.get() * 0xff + is.get())/((double)0xffff);
  // std::cout << "finished getting res" << "\n";
  return res;
}

double map(double x, double in_min, double in_max, double out_min, double out_max) {
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

std::uint32_t HSBtoRGB(int hue, int sat, int bright, const SDL_PixelFormat *format) {
  int colors[3];

  float max_rgb_val = 255;

  float sat_f = float(sat) / 100.0;
  float bright_f = float(bright) / 100.0;
  std::uint8_t r, g, b;

  if (bright <= 0) {
    colors[0] = 0;
    colors[1] = 0;
    colors[2] = 0;
  }

  if (sat <= 0) {
    colors[0] = bright_f * max_rgb_val;
    colors[1] = bright_f * max_rgb_val;
    colors[2] = bright_f * max_rgb_val;
  }

  else {
    if (hue >= 0 && hue < 120) {
      float hue_primary = 1.0 - (float(hue) / 120.0);
      float hue_secondary = float(hue) / 120.0;
      float sat_primary = (1.0 - hue_primary) * (1.0 - sat_f);
      float sat_secondary = (1.0 - hue_secondary) * (1.0 - sat_f);
      float sat_tertiary = 1.0 - sat_f;
      r = (bright_f * max_rgb_val) * (hue_primary + sat_primary);
      g = (bright_f * max_rgb_val) * (hue_secondary + sat_secondary);
      b = (bright_f * max_rgb_val) * sat_tertiary;
    }

    else if (hue >= 120 && hue < 240) {
      float hue_primary = 1.0 - ((float(hue) - 120.0) / 120.0);
      float hue_secondary = (float(hue) - 120.0) / 120.0;
      float sat_primary = (1.0 - hue_primary) * (1.0 - sat_f);
      float sat_secondary = (1.0 - hue_secondary) * (1.0 - sat_f);
      float sat_tertiary = 1.0 - sat_f;
      r = (bright_f * max_rgb_val) * sat_tertiary;
      g = (bright_f * max_rgb_val) * (hue_primary + sat_primary);
      b = (bright_f * max_rgb_val) * (hue_secondary + sat_secondary);
    }

    else // if (hue >= 240 && hue <= 360)
      {
      float hue_primary = 1.0 - ((float(hue) - 240.0) / 120.0);
      float hue_secondary = (float(hue) - 240.0) / 120.0;
      float sat_primary = (1.0 - hue_primary) * (1.0 - sat_f);
      float sat_secondary = (1.0 - hue_secondary) * (1.0 - sat_f);
      float sat_tertiary = 1.0 - sat_f;
      r = (bright_f * max_rgb_val) * (hue_secondary + sat_secondary);
      g = (bright_f * max_rgb_val) * sat_tertiary;
      b = (bright_f * max_rgb_val) * (hue_primary + sat_primary);
    }

    colors[0] = r;
    colors[1] = g;
    colors[2] = b;
  }

  r = colors[0];
  g = colors[1];
  b = colors[2];

  return SDL_MapRGBA(format, r, g, b, 0xff);
  return (b << 24) | (g << 16) | (r << 8) | 0x000000ff;
}

void draw_metaballs_threadpool(SDL_Texture *texture,
                               const std::vector<Ball> &balls, ctpl::thread_pool &pool, const SDL_PixelFormat * format) {
  void *pixels_void;
  int pitch;
  SDL_LockTexture(texture, nullptr, &pixels_void, &pitch);
  std::uint32_t *pixels = (std::uint32_t *)pixels_void;
  (void) pool;
  std::vector<std::future<void>> pool_results;

  for (int i = 0; i < WINDOW_WIDTH; ++i) {
    pool_results.push_back(pool.push([pixels, i, pitch, format, &balls](int) {
      for (int j = 0; j < WINDOW_HEIGHT; ++j) {
        pixels[idx(i, j)] = 0x00000000;

        double sum_dist = 0;
        for (const Ball &ball : balls) {
          int x = i - ball.x;
          int y = j - ball.y;
          sum_dist += 1.0 / std::sqrt(x * x + y * y);
        }

        if (sum_dist > 1.0 / BALL_RAD) {
          double recip = 1/sum_dist;
          std::uint32_t val = map(recip, 2.5, BALL_RAD, 0, 300);

          pixels[idx(i, j)] = HSBtoRGB(val, 100, 100, format);
        }
      }
    }));
  }

  for (auto &f : pool_results)
    f.wait();

  SDL_UnlockTexture(texture);
}

Ball rand_ball() {
  int x = WINDOW_WIDTH * get_rand();
  int y = WINDOW_HEIGHT * get_rand();
  int vx = get_rand() * 5 + 10 * (get_rand() < 0.5 ? -1 : 1);
  int vy = get_rand() * 5 + 10 * (get_rand() < 0.5 ? -1 : 1);
  return Ball {x, y, vx, vy};
}

void game_loop(bool *quit, std::vector<Ball> *balls) {
  auto start = std::chrono::high_resolution_clock::now(), now = start;
  SDL_Event event;
  while (!*quit) {
    SDL_PollEvent(&event);
    if (event.type == SDL_QUIT)
      *quit = true;

    else if (event.type == SDL_KEYDOWN) {
      fmt::print("key pressed: {}\n", event.key.keysym.scancode);
      if (event.key.keysym.scancode == SDL_SCANCODE_Q)
        *quit = true;
      else if (event.key.keysym.scancode == SDL_SCANCODE_A)
        balls->emplace_back(rand_ball());
      else if (event.key.keysym.scancode == SDL_SCANCODE_D)
        balls->erase(balls->begin());
    }

    now = std::chrono::high_resolution_clock::now();
    // if (now - start > 5s)
    //   *quit = true;

    std::this_thread::sleep_for(10ms);
  }
}

int main(int argc, char **argv) {
  (void) argc;
  (void) argv;
  SDL_Init(SDL_INIT_VIDEO);

  SDL_Window *window =
      SDL_CreateWindow("Metaballs", 10,
                       10, WINDOW_WIDTH, WINDOW_HEIGHT,
                       0
                       );

  SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

  SDL_Texture *texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ARGB32,
                                           SDL_TEXTUREACCESS_STREAMING,
                                           WINDOW_WIDTH, WINDOW_HEIGHT);

  std::vector<Ball> balls;

  for (int i = 0; i < NUM_BALLS; ++i) {
    balls.emplace_back(rand_ball());
  }
  
  bool quit = false;

  std::chrono::time_point<std::chrono::high_resolution_clock> last = std::chrono::high_resolution_clock::now(), now = std::chrono::high_resolution_clock::now();
  auto dt = now - last;
  // auto fps = 60;

  auto max_threads = std::thread::hardware_concurrency();
  fmt::print("max threads: {}\n", max_threads);
  ctpl::thread_pool pool(2);
  fmt::print("got the thread pool\n");

  std::thread event_thread(game_loop, &quit, &balls);

  auto pixel_format = SDL_AllocFormat(SDL_PIXELFORMAT_ARGB32);

  while (!quit) {
    last = std::chrono::high_resolution_clock::now();

    draw_metaballs_threadpool(texture, balls, pool, pixel_format);
    for (auto &b : balls) b.update(dt.count() / 100000000.0);

    SDL_RenderCopy(renderer, texture, nullptr, nullptr);
    SDL_RenderPresent(renderer);
    
    now = std::chrono::high_resolution_clock::now();
    dt = now - last;
    // fps = 1s / (now - last);

    auto sleep = 1000ms/30.0 - dt;
    if (sleep < 0s) sleep = 0s;
    std::this_thread::sleep_for(sleep);

    now = std::chrono::high_resolution_clock::now();
    dt = now - last;
    // fps = 1s / (now - last);
    
    // fmt::print("fps: {}, milliseconds: {}\n", fps, std::chrono::duration_cast<std::chrono::milliseconds>(dt).count());
    last = now;
  }

  SDL_FreeFormat(pixel_format);
  SDL_DestroyTexture(texture);
  SDL_DestroyRenderer(renderer);
  SDL_DestroyWindow(window);
  SDL_Quit();

  event_thread.join();

  return 0;
}
