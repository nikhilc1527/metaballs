CFLAGS = -O3 -Wall -Wextra -Werror -pedantic -std=c++20
LIBS = -lfmt -lSDL2 -pthread

all: main run

.PHONY: all

main: main.cpp
	$(CXX) $(CFLAGS) $(LIBS) main.cpp -o main

testudu: testudu.cpp
	$(CXX) $(CFLAGS) $(LIBS) testudu.cpp -o testudu

run: main
	./main

clean: main
	rm main
