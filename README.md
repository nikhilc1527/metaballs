# Metaballs

This is an animation that generates a random set of balls that bounce around the window. Instead of just having circles bouncing around (thats boring), each pixel is colored based on whether the sum of the inverse distances are greater than a threshold. Basically, the space in a circle around each ball, as well as the space in between the circles, are all colored in.

![](https://gitlab.com/nikhilc1527/metaballs/-/raw/master/gif.gif)
